// SPDX-License-Identifier: MIT

package theaudiodb

import (
	"fmt"
)

// ArtistResults is data artists return from theaudiodb API calls.
type ArtistResults struct {
	Artists []struct {
		ID            string `json:"idArtist"`
		Name          string `json:"strArtist"`
		NameStripped  string `json:"strArtistStripped"`
		NameAlt       string `json:"strArtistAlternate"`
		Label         string `json:"strLabel"`
		LabelID       string `json:"dLabel"`
		YearFormed    string `json:"intYearFormed"`
		YearBorn      string `json:"intYearBorn"`
		YearDied      string `json:"intYearDied"`
		YearDisbanded string `json:"strDisbanded"`
		Style         string `json:"strStyle"`
		Genre         string `json:"strGenre"`
		Mood          string `json:"strMood"`
		Website       string `json:"strWebsite"`
		Facebook      string `json:"strFacebook"`
		Twitter       string `json:"strTwitter"`
		BiographyEN   string `json:"strBiographyEN"`
		BiographyDE   string `json:"strBiographyDE"`
		BiographyFR   string `json:"strBiographyFR"`
		BiographyCN   string `json:"strBiographyCN"`
		BiographyIT   string `json:"strBiographyIT"`
		BiographyJP   string `json:"strBiographyJP"`
		BiographyRU   string `json:"strBiographyRU"`
		BiographyES   string `json:"strBiographyES"`
		BiographyPT   string `json:"strBiographyPT"`
		BiographyNL   string `json:"strBiographyNL"`
		BiographyHU   string `json:"strBiographyHU"`
		BiographyNO   string `json:"strBiographyNO"`
		BiographyIL   string `json:"strBiographyIL"`
		BiographyPL   string `json:"strBiographyPL"`
		Gender        string `json:"strGender"`
		Members       string `json:"intMembers"`
		County        string `json:"strCountry"`
		CountyCode    string `json:"strCountryCode"`
		Thumb         string `json:"strArtistThumb"`
		Logo          string `json:"strArtistLogo"`
		Clearart      string `json:"strArtistClearart"`
		ThumbWide     string `json:"strArtistWideThumb"`
		Fanart1       string `json:"strArtistFanart1"`
		Fanart2       string `json:"strArtistFanart2"`
		Fanart3       string `json:"strArtistFanart3"`
		Fanart4       string `json:"strArtistFanart4"`
		Banner        string `json:"strArtistBannder"`
		MusicBrainzID string `json:"strMusicBrainzID"`
		ISNI          string `json:"strISNIcode"`
		LastFMChart   string `json:"strLastFMChart"`
		Charted       string `json:"intCharted"`
		Locked        string `json:"strLocked"`
	} `json:"artists"`
} //struct

// ArtistSearch will for an artist using the provided string.
func ArtistSearch(name string) (artists ArtistResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("artists search: %w", err)
		} //if
	}() //func

	err = apiRequest("search.php", params("s", name), &artists)

	return
} //func

// ArtistByID will return an artist by their ID.
func ArtistByID(id string) (artists ArtistResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("artist by id: %w", err)
		} //if
	}() //func

	err = apiRequest("artist.php", params("i", id), &artists)

	return
} //func
