// SPDX-License-Identifer: MIT

package theaudiodb

import (
	"fmt"
)

// TrackResults is the tracks data returned from an audiodb API call.
type TrackResults struct {
	Tracks []struct {
		ID                  string `json:"idTrack"`
		AlbumID             string `json:"idAlbum"`
		ArtistID            string `json:"idArtist"`
		LyricID             string `json:"idLyric"`
		Name                string `json:"strTrack"`
		Album               string `json:"strAlbum"`
		Artist              string `json:"strArtist"`
		ArtistAlt           string `json:"strArtistAlternate"`
		CD                  string `json:"intCD"`
		Duration            string `json:"intDuration"`
		Genre               string `json:"strGenre"`
		Mood                string `json:"strMood"`
		Style               string `json:"strStyle"`
		Theme               string `json:"strTheme"`
		DescriptionEN       string `json:"strDescriptionEN"`
		DescriptionDE       string `json:"strDescriptionDE"`
		DescriptionFR       string `json:"strDescriptionFR"`
		DescriptionCN       string `json:"strDescriptionCN"`
		DescriptionIT       string `json:"strDescriptionIT"`
		DescriptionJP       string `json:"strDescriptionJP"`
		DescriptionRU       string `json:"strDescriptionRU"`
		DescriptionES       string `json:"strDescriptionES"`
		DescriptionPT       string `json:"strDescriptionPT"`
		DescriptionNL       string `json:"strDescriptionNL"`
		DescriptionHU       string `json:"strDescriptionHU"`
		DescriptionNO       string `json:"strDescriptionNO"`
		DescriptionIL       string `json:"strDescriptionIL"`
		DescriptionPL       string `json:"strDescriptionPL"`
		Thumb               string `json:"strTrackThumb"`
		Case3D              string `json:"strTrack3DCase"`
		Lyrics              string `json:"strTrackLyrics"`
		MusicVid            string `json:"strMusicVid"`
		MusicVidDirector    string `json:"strMusicVidDirectory"`
		MusicVidCompay      string `json:"strMuiscVidCompany"`
		MusicVidScreen1     string `json:"strMusicVidScreen1"`
		MusicVidScreen2     string `json:"strMusicVidScreen2"`
		MusicVidScreen3     string `json:"strMusicVidScreen3"`
		MusicVidScreen4     string `json:"strMusicVidScreen4"`
		MusicVidViews       string `json:"intMusicVidViews"`
		MusicVidLikes       string `json:"intMusicVidLikes"`
		MusicVidDislikes    string `json:"intMuiscVidDislikes"`
		MusicVidFavorites   string `json:"intMusicVidFavorites"`
		MusicVidComments    string `json:"intMusicVidComments"`
		TrackNumber         string `json:"intTrackNumber"`
		Loved               string `json:"intLoved"`
		Score               string `json:"intScore"`
		ScoreVotes          string `json:"intScoreVotes"`
		TotalListeners      string `json:"intTotalListeners"`
		TotalPlays          string `json:"intTotalPlays"`
		MusicBrainzID       string `json:"strMusicBrainzID"`
		MusicBrainzAlbumID  string `json:"strMusicBrainzAlbumID"`
		MusicBrainzArtistID string `json:"strMusicBrainzArtistID"`
		Locked              string `json:"strLocked"`
	} `json:"track"`
} //struct

// TracksByAlbumID returns tracks associated with and album's id.
func TracksByAlbumID(id string) (tracks TrackResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("tracks by album id: %w", err)
		} //if
	}() //func

	err = apiRequest("track.php", params("m", id), &tracks)

	return
} //func

// TrackByID returns the track with the provided id.
func TrackByID(id string) (tracks TrackResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("track by id: %w", err)
		} //if
	}() //func

	err = apiRequest("track.php", params("h", id), &tracks)

	return
} //func
