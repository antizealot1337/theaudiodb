// SPDX-License-Identifier

package theaudiodb

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

// APIKey is the API key to use. The value of "1" is the default and should
// only be used for testing. An actual API key should be provided for
// production/release.
var APIKey = "1"

// DefaultClient is the default http.Client. It will be used for requests to
// theaudiodb API.
var DefaultClient = http.DefaultClient

func apiRequest(resource string, params url.Values, out interface{}) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("api request: %w", err)
		} //func
	}() //func

	r, err := makeRequest(resource, params)
	if err != nil {
		return
	} //if
	defer r.Close()

	err = json.NewDecoder(r).Decode(out)

	return
} //func

func makeRequest(resource string, params url.Values) (r io.ReadCloser, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("make request: %w", err)
		} //if
	}() //func

	rurl := buildRequestURL(resource, params)

	req, err := http.NewRequest(http.MethodGet, buildRequestURL(resource, params), nil)
	if err != nil {
		return
	} //if

	resp, err := DefaultClient.Do(req)
	if err != nil {
		return
	} //if

	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		err = fmt.Errorf("bad status %s for %s", resp.Status, rurl)
		return
	} //if

	r = resp.Body

	return
} //func

func buildRequestURL(resource string, params url.Values) string {
	return fmt.Sprintf("https://theaudiodb.com/api/v1/json/%s/%s?%s", APIKey, resource, params.Encode())
} //func

func params(params ...string) url.Values {
	if len(params)%2 == 1 {
		panic("not enough params")
	} //if

	values := url.Values{}

	for i := 0; i < len(params); i += 2 {
		values.Add(params[i], params[i+1])
	} //for

	return values
} //func
