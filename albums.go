// SPDX-License-Identifier: MIT

package theaudiodb

import (
	"fmt"
)

// AlbumResults is the albums data returned from and audiodb API call.
type AlbumResults struct {
	Albums []struct {
		ID                 string `json:"idAlbum"`
		ArtistID           string `json:"idArtist"`
		LabelID            string `json:"idLabel"`
		Name               string `json:"strAlbum"`
		NameStripped       string `json:"strAlbumStripped"`
		ArtistName         string `json:"strArtist"`
		ArtistNameStripped string `json:"strArtistStripped"`
		YearReleased       string `json:"intYearReleased"`
		Style              string `json:"strStyle"`
		Genre              string `json:"strGenre"`
		Label              string `json:"strLabel"`
		ReleaseFormat      string `json:"strReleaseFormat"`
		Sales              string `json:"intSales"`
		Thumb              string `json:"strAlbumThumb"`
		ThumbHQ            string `json:"strAlbumThumbHQ"`
		ThumbBack          string `json:"strAlbumThumbBack"`
		CDart              string `json:"strAlbumCDart"`
		Spine              string `json:"strAlbumSpine"`
		Case3D             string `json:"strAlbum3DCase"`
		Flat3D             string `json:"strAlbum3DFlat"`
		Face3D             string `json:"strAlbum3DFace"`
		Thumb3D            string `json:"strAlbum3DThumb"`
		DescriptionEN      string `json:"strDescriptionEN"`
		DescriptionDE      string `json:"strDescriptionDE"`
		DescriptionFR      string `json:"strDescriptionFR"`
		DescriptionCN      string `json:"strDescriptionCN"`
		DescriptionIT      string `json:"strDescriptionIT"`
		DescriptionJP      string `json:"strDescriptionJP"`
		DescriptionRU      string `json:"strDescriptionRU"`
		DescriptionES      string `json:"strDescriptionES"`
		DescriptionPT      string `json:"strDescriptionPT"`
		DescriptionNL      string `json:"strDescriptionNL"`
		DescriptionHU      string `json:"strDescriptionHU"`
		DescriptionNO      string `json:"strDescriptionNO"`
		DescriptionIL      string `json:"strDescriptionIL"`
		DescriptionPL      string `json:"strDescriptionPL"`
		Loved              string `json:"intLoved"`
		Score              string `json:"intScore"`
		Votes              string `json:"intScoreVotes"`
		Review             string `json:"strReview"`
		Mood               string `json:"strMood"`
		Theme              string `json:"strTheme"`
		Speed              string `json:"strSpeed"`
		Location           string `json:"strLocation"`
		MusicBrainzID      string `json:"strMusicBrainzID"`
		MuscBrainzArtistID string `json:"strMusicBrainzArtistID"`
		AllMusicID         string `json:"strAllMusicID"`
		BBCReviewID        string `json:"strBBCReviewID"`
		RateYourMusicID    string `json:"strRateYourMusicID"`
		DiscogsID          string `json:"strDiscogsID"`
		WikidataID         string `json:"strWikidataID"`
		WikipediaID        string `json:"strWikipediaID"`
		GeniusID           string `json:"strGeniusID"`
		LyricWikiID        string `json:"strLyricWikiID"`
		MusicMozID         string `json:"strMusicMozID"`
		ItunesID           string `json:"strItunesID"`
		AmazonID           string `json:"strAmazonID"`
		Locked             string `json:"strLocked"`
	} `json:"album"`
} //struct

// AlbumsByArtistID returns albums associated with an artist's id.
func AlbumsByArtistID(id string) (albums AlbumResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("albums by artist id: %w", err)
		} //if
	}() //func

	err = apiRequest("album.php", params("i", id), &albums)

	return
} //func

// AlbumByID returns the album with the provided id.
func AlbumByID(id string) (albums AlbumResults, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("albums by artist id: %w", err)
		} //if
	}() //func

	err = apiRequest("album.php", params("m", id), &albums)

	return
} //func
