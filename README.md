# TheAudioDB

Go library to access [theaudiodb.com][theaudiodb] API. Since some of the API is
behind a Patreon paywall this api is incomplete ([API Docs][api docs]).

## Usage

### Artists

Search for artist by name example:

```go
results, err := theaudiodb.ArtistSearch("name")
// Check error

for _, artist := range results.Artists {
	// Display artist
}
```

Search for an artist by id example:

```go
results, err := theaudiodb.ArtistByID("1") // NOTE: IDs are strings
// Check error

for _, artist := range results.Artists {
	// Display artist
}
```
### Albums

Get all albums by an artist (id) example:

```go
results, err := theaudiodb.AlbumsByArtistID("1") // NOTE: This artist id is a string
// Check error

for _, album := range results.Albumns {
	// Display album
}
```

Get an album by id example:

```go
results, err := theaudiodb.AlbumByID("1") // NOTE: This id is a string
// Check error

for _, album := range results.Albumns {
	// Display album
}
```
### Tracks

Get all tracks by an album (id) example:

```go
results, err := theaudiodb.TracksByAlbumID("1") // NOTE: This album id is a string
// Check error

for _, track := range results.Tracks {
	// Display track
}
```

Get a track by id example:
```go
results, err := theaudiodb.TrackByID("1") // NOTE: This id is a string
// Check error

for _, track := range results.Tracks {
	// Display track
}
```

## License

Licensed under the terms of the MIT license. Please refer to LICENSE for
detials.

[theaudiodb]: https://theaudiodb.com
[api docs]: https://theaudiodb.com/api_guide.php
