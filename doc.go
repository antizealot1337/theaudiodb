// SPDX-License-Identifer: MIT

// An incomplete wrapper around API at TheAudioDB.com. Many of the freely
// available API enpoints have been implemented. Those requiring a Patreon
// account have been excluded for now.
//
// This package uses the freely available API token/key of "1". To use another
// key call `theaudiodb.APIKey = <some value>`.
//
// Similarly the package uses http.DefaultClient for requests. Another client
// may be used with `theaudiodb.DefaultClient = http.Client{ /*set fields*/ }`.
package theaudiodb
